﻿using System;
using System.Collections.Generic;
using System.Linq;
using BoardGame.Models;

namespace BoardGame
{
    public class ColorRanker
    {
        private List<Color> _colorsRanked;
        private Random _random;

        public ColorRanker(IList<Color> colorsRanked)
        {
            if (colorsRanked == null || colorsRanked.Count == 0)
                throw new ArgumentException("colorsRanked");
            _colorsRanked = colorsRanked.ToList();
            _random = new Random(DateTime.Now.Millisecond);
        }

        public Color PickRandom()
        {
            return _colorsRanked[_random.Next(0, _colorsRanked.Count)];
        }

        public Color PickLowest()
        {
            return _colorsRanked[0];
        }

        public Color PickLowest(params Color[] colors)
        {
            var lowest = Int32.MaxValue;
            foreach (var color in colors)
            {
                var idx = _colorsRanked.IndexOf(color);
                if (idx > 0 && idx < lowest)
                    lowest = idx;
            }

            return lowest != Int32.MaxValue ? _colorsRanked[lowest] : null;
        }

        public Color PickBest(IDictionary<Color, int> weights)
        {
            return weights.OrderByDescending(pair => pair.Value)
                   .ThenBy(pair => _colorsRanked.IndexOf(pair.Key)).Select(pair=>pair.Key)
                   .FirstOrDefault();
        }
    }
}
