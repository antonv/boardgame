﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BoardGame.Player
{
    public class AutoPlayer
    {
        public IPlayStrategy PlayStrategy { get; set; }
        
        public AutoPlayer(IPlayStrategy playStrategy)
        {
            PlayStrategy = playStrategy;
        }


        public void Play(Models.Board board)
        {
            PlayStrategy.Init(board);
            var strategy = PlayStrategy;

            int step = 0;
            bool wasOk = false;
            do
            {
                Console.WriteLine(board);
                wasOk = strategy.TryStep();
                if (wasOk)
                {
                    step++;
                }

            } while (wasOk);

            Console.WriteLine("Game finished in {0} steps", step);
        }
    }
}
