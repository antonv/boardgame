﻿using System;
using System.Collections.Generic;
using System.Linq;
using BoardGame.Models;

namespace BoardGame.Player
{
    public interface IBoardScanner
    {
        void Init(Board board);
        IDictionary<Color, Area> GetNeighbourAreas(Area area);
        void ExpandArea(Area area);
    }

    public class DefaultBoardScanner : IBoardScanner
    {
        private Board _board;

        public DefaultBoardScanner()
        {
        }

        public DefaultBoardScanner(Board board)
        {
            _board = board;
        }

        public void Init(Board board)
        {
            _board = board;
        }

        public IDictionary<Color, Area> GetNeighbourAreas(Area area)
        {
            EnsureInitialized();
            var areasByColor = new Dictionary<Color, Area>();
            // should have iterative steps to expand the area for each color
            var neighbours = _board.GetNeighbours(area);
            foreach (var neighbour in neighbours)
            {
                var color = _board[neighbour].Color;
                Area colorArea; 
                if (!areasByColor.ContainsKey(color))
                {
                    areasByColor.Add(color, colorArea = new Area(color));
                }
                else
                {
                    colorArea = areasByColor[color];
                }
                colorArea.Add(neighbour);
            }

            return areasByColor;
        }

        public void ExpandArea(Area area)
        {
            while(true)
            {
                var nearPoints = _board.GetNeighbours(area, true);
                if (nearPoints.Count == 0)
                    break;
                area.AddRange(nearPoints);
            } 
        }

        private void EnsureInitialized()
        {
            if (_board == null)
            {
                throw new InvalidOperationException("Board Scanner not initialized with board instance");
            }
        }
    }

    public class OptimizedBoardScanner : IBoardScanner
    {
        public void Init(Board board)
        {
            throw new NotImplementedException();
        }

        public IDictionary<Color, Area> GetNeighbourAreas(Area area)
        {
            throw new NotImplementedException();
        }

        public void ExpandArea(Area area)
        {
            throw new NotImplementedException();
        }
    }
}