﻿using System;
using System.Linq;
using BoardGame.Models;
using NUnit.Framework.Constraints;

namespace BoardGame.Player
{
    public class GreedyStrategy : IPlayStrategy
    {
        private Models.Board _board;
        private Area _currentArea;
        private IBoardScanner _boardScanner;
        private ColorRanker _colorRanker;

        public IBoardScanner BoardScanner
        {
            get { return _boardScanner; }
            set
            {
                if (value != null && _boardScanner != value)
                {
                    _boardScanner = value;
                    _boardScanner.Init(_board);
                }
            }
        }


        public GreedyStrategy(ColorRanker ranker)
        {
            if (ranker == null)
                throw new ArgumentNullException("ranker");
            _colorRanker = ranker;
            _boardScanner = new DefaultBoardScanner();
        }

        public void Init(Board board)
        {
            _board = board;
            _currentArea = new Area(_board[0, 0].Color);
            _currentArea.Add(new Point(0, 0));
            if (_boardScanner == null)
            {
                _boardScanner = new DefaultBoardScanner();
            }

            _boardScanner.Init(_board);
        }

        public bool TryStep()
        {
            var neighbourAreas = _boardScanner.GetNeighbourAreas(_currentArea);

            if (neighbourAreas == null || neighbourAreas.Count == 0) return false;

            foreach (var area in neighbourAreas.Values)
            {
                _boardScanner.ExpandArea(area);
            }

            var weightedColors = neighbourAreas.ToDictionary(a => a.Key, a => a.Value.Count);
            var bestColor = _colorRanker.PickBest(weightedColors);


            _currentArea.Color = bestColor;
            _board.SetColor(_currentArea);

            _currentArea.Add(neighbourAreas[bestColor]);
            return true;
        }
    }
}