﻿using BoardGame.Models;

namespace BoardGame.Player
{
    public interface IPlayStrategy
    {
        void Init(Board board);
        bool TryStep();
    }

    public interface IPlayStrategyFactory
    {
        IPlayStrategy CreateStrategy();
    }
}