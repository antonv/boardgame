﻿using System;
using System.Collections.Generic;

namespace BoardGame.Models
{
    public class Cell
    {
        public Color Color { get; set; }

        public Cell(Color color)
        {
            Color = color;
        }

        public override string ToString()
        {
            return Color != null ? Color.ToString() : "?";
        }
    }
}
