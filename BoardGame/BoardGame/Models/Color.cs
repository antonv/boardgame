﻿namespace BoardGame.Models
{
    public class Color
    {
        public string Key { get; private set; }
        public string Name { get; private set; }

        public Color(string key, string name)
        {
            Key = key;
            Name = name;
        }

        public override string ToString()
        {
            return Key;
        }
    }
}
