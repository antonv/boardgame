﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BoardGame.Models
{
    public class Area: IEnumerable<Point>
    {
        private Color _color;
        private List<Point> _points;

        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public int Count
        {
            get { return _points.Count; }
        }

        public Area(Color color)
        {
            _color = color;
            _points = new List<Point>();
        }

        public Area Add(int x, int y)
        {
            return Add(new Point(x, y));
        }
        
        public Area Add(Point point)
        {
            var containsSame = _points.Any(pt => pt.X == point.X && pt.Y == point.Y);
            if (!containsSame)
            {
                _points.Add(point);
            }
            return this;
        }

        public Area AddRange(IList<Point> points)
        {
            foreach (var point in points)
            {
                Add(point);
            }
            return this;
        }

        public Area Add(Area area)
        {
            foreach (var point in area)
            {
                Add(point);
            }
            return this;
        }

        public bool Included(Point point)
        {
            return _points.Any(pt => pt.X == point.X && pt.Y == point.Y);
        }

        public IEnumerator<Point> GetEnumerator()
        {
            return _points.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _points.GetEnumerator();
        }
    }

    public struct Point
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        
        public Point(int x, int y) : this()
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return string.Format("[{0}, {1}]", X, Y);
        }
    }
}
