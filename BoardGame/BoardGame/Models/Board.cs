﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BoardGame.Models
{
    public class Board
    {
        private Cell[,] _board;
        private int _boardSize;

        public Cell this[int x, int y]
        {
            get { return _board[x, y]; }
        }

        public Cell this[Point pt]
        {
            get { return _board[pt.X, pt.Y]; }
        }

        public int Size
        {
            get { return _boardSize; }
        }

        public Board(int boardSize)
        {
            if (boardSize <= 0)
                throw new ArgumentOutOfRangeException("boardSize");

            _boardSize = boardSize;
            _board = new Cell[boardSize, boardSize];
            for (int i = 0; i < _boardSize; i++)
            {
                for (int j = 0; j < _boardSize; j++)
                {
                    _board[i, j] = new Cell(null);
                }
            }
        }

        public Board SetColor(int x, int y, Color color)
        {
            var cell = _board[x, y];
            cell.Color = color;
            return this;
        }

        public void SetColor(Area area)
        {
            foreach (var point in area)
            {
                _board[point.X, point.Y].Color = area.Color;
            }
        }

        public List<Point> GetNeighbours(Point point, bool sameColor = false)
        {
            var x = point.X;
            var y = point.Y;
            var res = new List<Point>();
            var pointColor = this[point].Color;
            if (x - 1 >= 0)
                AddColorAware(res, point, new Point(x - 1, y), sameColor);
            if (x + 1 < _boardSize)
                AddColorAware(res, point, new Point(x + 1, y), sameColor);
            if (y - 1 >= 0)
                AddColorAware(res, point, new Point(x, y - 1), sameColor);
            if (y + 1 < _boardSize)
                AddColorAware(res, point, new Point(x, y + 1), sameColor);
            return res;
        }

        private void AddColorAware(List<Point> list, Point initPoint, Point pointToAdd, bool sameColor)
        {
            if (!sameColor || this[pointToAdd].Color == this[initPoint].Color)
                list.Add(pointToAdd);
        }

        public IList<Point> GetNeighbours(Area area, bool sameColor = false)
        {
            var foundNeighbours = new List<Point>();

            foreach (var cell in area)
            {
                var neighbours = GetNeighbours(cell, sameColor);
                foreach (var neighbour in neighbours)
                {

                    if (!area.Included(neighbour) && !foundNeighbours.Contains(neighbour))
                        foundNeighbours.Add(neighbour);
                }
            }
            return foundNeighbours;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            for (int i = 0; i < _boardSize; i++)
            {
                for (int j = 0; j < _boardSize; j++)
                {
                    builder.AppendFormat("{0}  ", _board[i, j]);
                }
                builder.AppendLine();
            }
            return builder.ToString();
        }
    }
}
