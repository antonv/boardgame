﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoardGame.Models;
using NUnit.Framework;

namespace BoardGame.Tests
{
    [TestFixture]
    public class BoardTests
    {
        [Test]
        public void Create_Board()
        {
            var board = new Board(2);

            Assert.AreEqual(2, board.Size);
            Assert.NotNull(board[0, 0]);
            Assert.Null(board[0, 0].Color);
            Assert.NotNull(board[1, 1]);
            Assert.Null(board[1, 1].Color);
        }

        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_Board_wrong_size()
        {
            var board = new Board(-1);
        }

        [Test]
        public void Set_Color()
        {
            var board = new Board(2);

            var cell = board[0, 0];
            var gray = new Color("gr", "gray");

            board.SetColor(0, 0, gray);

            Assert.AreSame(cell, board[0, 0]);
            Assert.AreSame(gray, board[0, 0].Color);
        }

        [Test]
        public void Change_Color()
        {
            var board = new Board(2);

            var cell = board[0, 0];
            var gray = new Color("gr", "gray");
            board.SetColor(0, 0, gray);

            var red = new Color("r", "red");
            board.SetColor(0, 0, red);

            Assert.AreSame(cell, board[0, 0]);
            Assert.AreSame(red, board[0, 0].Color);
        }

        [Test]
        public void Neighbours_Corners()
        {
            var c1 = new Color("1", "1");
            var c2 = new Color("2", "2");
            var c3 = new Color("3", "3");
            var c4 = new Color("4", "4");

            // c1 | c2
            // c3 | c4
            var board = new Board(2);
            board.SetColor(0, 0, c1).SetColor(0, 1, c2).SetColor(1, 0, c3).SetColor(1, 1, c4);

            var neighbours = board.GetNeighbours(new Point(0, 0));

            Assert.AreEqual(2, neighbours.Count);
            Assert.NotNull(neighbours.Select(pt=>board[pt]).Single(c => c.Color == c2));
            Assert.NotNull(neighbours.Select(pt => board[pt]).Single(c => c.Color == c3));

            neighbours = board.GetNeighbours(new Point(0, 1));

            Assert.AreEqual(2, neighbours.Count);
            Assert.NotNull(neighbours.Select(pt => board[pt]).Single(c => c.Color == c1));
            Assert.NotNull(neighbours.Select(pt => board[pt]).Single(c => c.Color == c4));

        }

        [Test]
        public void Neighbours_Center()
        {
            var c1 = new Color("1", "1");
            var c2 = new Color("2", "2");
            var c3 = new Color("3", "3");

            // c1 | c2 | c1
            // c2 | c3 | c2
            // c1 | c2 | c1
            var board = new Board(3);
            board.SetColor(0, 0, c1).SetColor(0, 1, c2).SetColor(0, 2, c1)
                 .SetColor(1, 0, c2).SetColor(1, 1, c3).SetColor(1, 2, c2)
                 .SetColor(2, 0, c1).SetColor(2, 1, c2).SetColor(2, 2, c1);

            var neighbours = board.GetNeighbours(new Point(1, 1));

            Assert.AreEqual(4, neighbours.Count);
            Assert.True(neighbours.Select(pt => board[pt]).All(c => c.Color == c2));
        }

        [Test]
        public void Neighbours_Of_Area()
        {
            var c1 = new Color("1", "1");
            var c2 = new Color("2", "2");
            var c3 = new Color("3", "3");

            // c1 | c1 | c1
            // c2 | c2 | c1
            // c3 | c3 | c2
            var board = new Board(3);
            board.SetColor(0, 0, c1).SetColor(0, 1, c1).SetColor(0, 2, c1)
                 .SetColor(1, 0, c2).SetColor(1, 1, c2).SetColor(1, 2, c1)
                 .SetColor(2, 0, c3).SetColor(2, 1, c2).SetColor(2, 2, c2);

            var area = new Area(board[0, 0].Color);
            area.Add(0, 0).Add(0, 1).Add(0, 2).Add(1, 2);

            var neighbours = board.GetNeighbours(area);

            Assert.AreEqual(3, neighbours.Count);
            Assert.True(neighbours.Select(pt => board[pt]).All(c => c.Color == c2));
        }

        [Test]
        public void ToString_Not_fails()
        {
            var board = new Board(2);
            board.ToString();
        }
    }
}
