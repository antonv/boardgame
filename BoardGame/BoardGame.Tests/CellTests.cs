﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoardGame.Models;
using NUnit.Framework;

namespace BoardGame.Tests
{
    [TestFixture]
    public class CellTests
    {
        [Test]
        public void Create_Cell()
        {
            var red = new Color("r", "red");
            var cell = new Cell(red);

            Assert.AreSame(red, cell.Color);
        }

        [Test]
        public void Create_NoColor()
        {
            var cell = new Cell(null);

            Assert.IsNull(cell.Color);
        }

        [Test]
        public void ToString_NotFails()
        {
            var red = new Color("r", "red");
            var cell = new Cell(red);
            var cell2 = new Cell( null);

            var str = cell.ToString();
            var str2 = cell2.ToString();
        }
    }
}
