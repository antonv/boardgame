﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoardGame.Models;
using BoardGame.Player;
using NUnit.Framework;

namespace BoardGame.Tests
{
    [TestFixture]
    public class AutoPlayerIntegrationTests
    {
        private ColorRanker _colorRanker;

        [TestFixtureSetUp]
        public void Setup()
        {
            _colorRanker = new ColorRanker(new[]
            {
                new Color("0", "Red"),
                new Color("1", "Green"),
                new Color("2", "Blue"), 
            });
        }

        [Test]
        public void PlaySmallBoard()
        {
            var board = new Board(2);
            FillBoardRandom(board);


            var player = new AutoPlayer(new GreedyStrategy(_colorRanker));
            player.Play(board);
        }

        [Test]
        public void PlayBigBoard()
        {
            var board = new Board(3);
            FillBoardRandom(board);

            var player = new AutoPlayer(new GreedyStrategy(_colorRanker));
            player.Play(board);
        }

        private void FillBoardRandom(Board board)
        {
            for (int i = 0; i < board.Size; i++)
            {
                for (int j = 0; j < board.Size; j++)
                {
                    board.SetColor(i,j, _colorRanker.PickRandom());
                }
            }
        }
    }
}
